/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of the {@link JacocoCounters}.
 */
public class JacocoCountersTest
{
    @Test
    public void testDefaults()
    {
        JacocoCounters underTest = new JacocoCounters();
        Assert.assertTrue(underTest.isShowBranchCoverage());
        Assert.assertTrue(underTest.isShowClassCoverage());
        Assert.assertTrue(underTest.isShowMethodCoverage());
        Assert.assertTrue(underTest.isShowLineCoverage());
        Assert.assertTrue(underTest.isShowInstructionCoverage());
        Assert.assertTrue(underTest.isShowComplexityCoverage());
        Assert.assertTrue(underTest.isCounterEnabled(JacocoCounterType.Line));
        Assert.assertTrue(underTest.isCounterEnabled(JacocoCounterType.Class));
    }



    @Test
    public void testMixed()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.setShowBranchCoverage(false);
        underTest.setShowClassCoverage(true);
        underTest.setShowMethodCoverage(true);
        underTest.setShowLineCoverage(true);
        underTest.setShowInstructionCoverage(false);
        underTest.setShowComplexityCoverage(true);

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertTrue(underTest.isShowClassCoverage());
        Assert.assertTrue(underTest.isShowMethodCoverage());
        Assert.assertTrue(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertTrue(underTest.isShowComplexityCoverage());
        Assert.assertTrue(underTest.isCounterEnabled(JacocoCounterType.Line));
        Assert.assertFalse(underTest.isCounterEnabled(JacocoCounterType.Instruction));
    }



    @Test
    public void testOnlyBranchCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showBranchCoverageOnly();

        Assert.assertTrue(underTest.isShowBranchCoverage());
        Assert.assertFalse(underTest.isShowClassCoverage());
        Assert.assertFalse(underTest.isShowMethodCoverage());
        Assert.assertFalse(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertFalse(underTest.isShowComplexityCoverage());
    }



    @Test
    public void testOnlyClassCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showClassCoverageOnly();

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertTrue(underTest.isShowClassCoverage());
        Assert.assertFalse(underTest.isShowMethodCoverage());
        Assert.assertFalse(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertFalse(underTest.isShowComplexityCoverage());
    }



    @Test
    public void testOnlyMethodCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showMethodCoverageOnly();

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertFalse(underTest.isShowClassCoverage());
        Assert.assertTrue(underTest.isShowMethodCoverage());
        Assert.assertFalse(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertFalse(underTest.isShowComplexityCoverage());
    }



    @Test
    public void testOnlyLineCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showLineCoverageOnly();

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertFalse(underTest.isShowClassCoverage());
        Assert.assertFalse(underTest.isShowMethodCoverage());
        Assert.assertTrue(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertFalse(underTest.isShowComplexityCoverage());
    }



    @Test
    public void testOnlyInstructionCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showInstructionCoverageOnly();

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertFalse(underTest.isShowClassCoverage());
        Assert.assertFalse(underTest.isShowMethodCoverage());
        Assert.assertFalse(underTest.isShowLineCoverage());
        Assert.assertTrue(underTest.isShowInstructionCoverage());
        Assert.assertFalse(underTest.isShowComplexityCoverage());
    }



    @Test
    public void testOnlyComplexityCoverage()
    {
        JacocoCounters underTest = new JacocoCounters();
        underTest.showComplexityCoverageOnly();

        Assert.assertFalse(underTest.isShowBranchCoverage());
        Assert.assertFalse(underTest.isShowClassCoverage());
        Assert.assertFalse(underTest.isShowMethodCoverage());
        Assert.assertFalse(underTest.isShowLineCoverage());
        Assert.assertFalse(underTest.isShowInstructionCoverage());
        Assert.assertTrue(underTest.isShowComplexityCoverage());
    }
}
