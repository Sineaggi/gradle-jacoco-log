/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.testing.jacoco.plugins.JacocoPlugin;
import org.gradle.testing.jacoco.tasks.JacocoReport;
import org.gradle.tooling.UnsupportedVersionException;
import org.gradle.util.GradleVersion;


/**
 * A Gradle plugin that aggregates and logs JaCoCo test coverage.
 */
public class JacocoLogPlugin
    implements Plugin<Project>
{
    public static final String PLUGIN_ID = "org.barfuin.gradle.jacocolog";

    private static final GradleVersion MIN_GRADLE_VERSION = GradleVersion.version("5.0");

    private static final Pattern TASKNAME_PATTERN = Pattern.compile("jacoco(.+?)Report");

    private static final AtomicInteger COUNTER = new AtomicInteger();



    public void apply(@Nonnull final Project pProject)
    {
        pProject.getPluginManager().apply(JacocoPlugin.class);

        pProject.getLogger().info("Applying plugin '" + PLUGIN_ID + "'");
        checkMinGradleVersion();
        new JacocoAggReportConfigAction().execute(pProject);
        pProject.getTasks().withType(JacocoReport.class, reportTask -> {
            String logTaskName = getLogTaskName(pProject, reportTask.getName());
            registerTask(pProject, reportTask, logTaskName);
        });
    }



    private void checkMinGradleVersion()
    {
        if (GradleVersion.current().compareTo(MIN_GRADLE_VERSION) < 0) {
            throw new UnsupportedVersionException("The plugin '" + PLUGIN_ID + "' requires at least Gradle "
                + MIN_GRADLE_VERSION.getVersion() + " to be run.");
        }
    }



    private String getLogTaskName(final Project pProject, final String pReportTaskName)
    {
        Matcher matcher = TASKNAME_PATTERN.matcher(pReportTaskName);
        if (matcher.matches()) {
            return "jacocoLog" + capitalize(matcher.group(1)) + "Coverage";
        }

        // The JaCoCo plugin uses the following naming convention:        @formatter:off
        // https://github.com/gradle/gradle/blob/v7.4.2/subprojects/jacoco/src/main/java/org/gradle/testing/jacoco/plugins/JacocoPlugin.java#L245
        // @formatter:on
        final String fallbackName = "jacocoLogCoverage" + COUNTER.incrementAndGet();
        pProject.getLogger().warn("WARNING: The task '" + pReportTaskName
            + "' of type 'JacocoReport' does not follow the naming convention \"jacoco(TestTask)Report\", where "
            + "\"TestTask\" is the capitalized name of the test task covered by the report. We improvise by using '"
            + fallbackName + "' as a fallback task name.");
        return fallbackName;
    }



    private void registerTask(final Project pProject, final JacocoReport pReportTask, final String pLogTaskName)
    {
        final TaskContainer tasks = pProject.getTasks();
        TaskProvider<LogCoverageTask> taskProvider = tasks.register(pLogTaskName, LogCoverageTask.class);
        taskProvider.configure((LogCoverageTask task) -> task.setReportTask(pReportTask));
        pReportTask.finalizedBy(taskProvider);
    }



    private String capitalize(String pStr)
    {
        return !pStr.isEmpty() ? (Character.toTitleCase(pStr.charAt(0)) + pStr.substring(1)) : pStr;
    }
}
