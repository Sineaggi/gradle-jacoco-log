/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.xml.parsers.ParserConfigurationException;

import groovy.lang.Closure;
import groovy.util.Node;
import groovy.util.NodeList;
import org.apache.commons.lang3.StringUtils;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Nested;
import org.gradle.api.tasks.TaskAction;
import org.gradle.testing.jacoco.tasks.JacocoReport;
import org.xml.sax.SAXException;


/**
 * The Gradle task that logs the coverage information is added by the plugin for each JacocoReport task.
 */
public class LogCoverageTask
    extends DefaultTask
{
    private static final DecimalFormatSymbols NUMBER_FORMAT_LOCALE = new DecimalFormatSymbols(Locale.ENGLISH);

    private boolean logAlways = true;

    private final JacocoCounters counters = new JacocoCounters();

    private final Property<Integer> maxDecimalDigits;

    private JacocoReport reportTask = null;



    public LogCoverageTask()
    {
        setGroup(JavaBasePlugin.VERIFICATION_GROUP);
        maxDecimalDigits = getProject().getObjects().property(Integer.class);
        maxDecimalDigits.set(Integer.valueOf(1));
    }



    @Input
    public boolean isLogAlways()
    {
        return logAlways;
    }



    public void setLogAlways(final boolean pLogAlways)
    {
        logAlways = pLogAlways;
    }



    @Input
    @Nonnull
    public Property<Integer> getMaxDecimalDigits()
    {
        return maxDecimalDigits;
    }



    private int getMaxDecimalDigitsAdjusted()
    {
        if (getMaxDecimalDigits().get() < 0) {
            getLogger().warn("WARNING: Invalid value (" + getMaxDecimalDigits().getOrNull() + ") in " + getName()
                + ".maxDecimalDigits, defaulting to 1");
            return 1;
        }
        else if (getMaxDecimalDigits().get() > 10) {
            return 10;
        }
        return getMaxDecimalDigits().get();
    }



    @Nested
    public JacocoCounters getCounters()
    {
        return counters;
    }



    /**
     * Configures the counters to be logged by this task.
     *
     * @param pClosure a configuration closure for a JacocoCounters object
     * @return the configured object
     */
    public JacocoCounters counters(final Closure<JacocoCounters> pClosure)
    {
        pClosure.setDelegate(counters);
        pClosure.call();
        return counters;
    }



    /**
     * Configures the counters to be logged by this task.
     *
     * @param pConfigureAction a configuration action for a JacocoCounters object
     * @return the configured object
     */
    public JacocoCounters counters(final Action<? super JacocoCounters> pConfigureAction)
    {
        pConfigureAction.execute(counters);
        return counters;
    }



    public void setReportTask(final JacocoReport pReportTask)
    {
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "Configuring log task '" + getName() + "' for report task '" + pReportTask.getName() + "'");
        }
        reportTask = pReportTask;
        setDescription("Logs JaCoCo test coverage from report task '" + pReportTask.getName() + "'.");
        dependsOn(pReportTask);
        enableXmlReport(pReportTask);
    }



    private void enableXmlReport(final JacocoReport pReportTask)
    {
        pReportTask.configure(new Closure<Void>(this)
        {
            @Override
            @SuppressWarnings("MethodDoesntCallSuperMethod")
            public Void call()
            {
                new ReportProxy(((JacocoReport) getDelegate()).getReports().getXml()).setEnabled(true);
                return null;
            }
        });
    }



    @TaskAction
    public void logCoverage()
    {
        if (reportTask == null) {
            throw new GradleException("Bug: Report task not set for task '" + getName() + "'");
        }

        ReportProxy xmlReport = new ReportProxy(reportTask.getReports().getXml());
        if (!xmlReport.isEnabled()) {
            getLogger().error("ERROR: XML report not enabled for task '" + reportTask.getName()
                + "'. No coverage can be logged.");
            return;
        }

        final int mdd = getMaxDecimalDigitsAdjusted();

        File reportXmlFile = xmlReport.getOutputFile();
        if (reportXmlFile != null && reportFileShouldExist(reportXmlFile)) {
            final NodeList counters = readCounters(reportXmlFile);
            if (!counters.isEmpty()) {
                getLogger().lifecycle("Test Coverage:");
                for (JacocoCounterType type : JacocoCounterType.values()) {
                    if (getCounters().isCounterEnabled(type)) {
                        logCoverage(counters, type, mdd);
                    }
                }
            }
        }
    }



    /**
     * Determine if the status of the build is such that a report XML file should exist for us to read. If this is not
     * the case, this whole task will do nothing.
     *
     * @param pReportXmlFile the potential report XML file, for checking its existence
     * @return <code>true</code> if we should proceed, of <code>false</code> if we should do nothing
     */
    /* The decision is reached like this (PlantUML):
     *
     * @startuml
     * :reportFileShouldExist();
     * if (Was the report task executed?) then (yes)
     *   if (XML file exists?) then (yes)
     *     #Transparent:Report on the file\nthat we have, which\nis current;
     *     #LightGreen:true;
     *     end
     *   else (no)
     *     #LightPink:ERROR, will throw\nFileNotFoundException\nlater;
     *     #LightGreen:true;
     *     end
     *   endif;
     * else (no)
     *   if (Log always?) then (yes)
     *     if (XML file exists?) then (yes)
     *       #Transparent:Report on the file\nthat we have, which\nis from previous run;
     *       #LightGreen:true;
     *       end
     *     else (no)
     *       #Transparent:skip this task, too;
     *       #LightGray:false;
     *       end
     *     endif;
     *   else (no)
     *     #Transparent:do nothing;
     *     #LightGray:false;
     *     end
     *   endif;
     * endif;
     * @enduml
     */
    private boolean reportFileShouldExist(final File pReportXmlFile)
    {
        final boolean reportTaskExecuted = reportTask.getDidWork();
        final boolean xmlFileExists = pReportXmlFile.canRead();

        if (!reportTaskExecuted) {
            String msg = getName() + ": The report task which this task is associated with ('" + reportTask.getPath()
                + "') did not perform any work. ";
            if (xmlFileExists && isLogAlways()) {
                msg += "Still, the report XML exists, so we will report on that.";
            }
            else {
                if (xmlFileExists) {
                    msg += "Consequently, we skip coverage logging, too.";
                }
                else {
                    msg += "Consequently, no report XML exists, so we skip coverage logging, too.";
                }
            }
            getLogger().info(msg);
        }
        return reportTaskExecuted || (xmlFileExists && isLogAlways());
    }



    @Nonnull
    private NodeList readCounters(@Nonnull final File pReportXmlFile)
    {
        try {
            Node root = new ParserProxy().parse(pReportXmlFile);
            NodeList counters = (NodeList) root.get("counter");
            if (counters == null || counters.isEmpty()) {
                getLogger().warn("WARNING: No counters found in JaCoCo XML report: " + pReportXmlFile);
                if (counters == null) {
                    counters = new NodeList();
                }
            }
            return counters;
        }
        catch (FileNotFoundException e) {
            throw new GradleException("JaCoCo XML report not found: " + pReportXmlFile, e);
        }
        catch (ParserConfigurationException | SAXException | IOException | RuntimeException e) {
            throw new GradleException("Failed to parse JaCoCo XML report: " + pReportXmlFile, e);
        }
    }



    private void logCoverage(@Nonnull final NodeList pCounters, @Nonnull final JacocoCounterType pType,
        final int pMaxDecimalDigits)
    {
        Optional<Node> counter = findCounter(pCounters, pType);
        double cov = counter.map(this::getCoverageFromCounter).orElse(100.0d);
        getLogger().lifecycle("    - " + pType + " Coverage: " + formatNumber(pMaxDecimalDigits, cov) + "%");
    }



    String formatNumber(final int pMaxDecimalDigits, final double pValue)
    {
        String format = "#" + (pMaxDecimalDigits == 0 ? "" : ".") + StringUtils.repeat('#', pMaxDecimalDigits);
        return new DecimalFormat(format, NUMBER_FORMAT_LOCALE).format(pValue);
    }



    private Optional<Node> findCounter(@Nonnull final NodeList pCounters, @Nonnull final JacocoCounterType pType)
    {
        Node result = null;
        for (Object nodeObj : pCounters) {
            if (nodeObj instanceof Node) {
                Node node = (Node) nodeObj;
                if (pType.getType().equals(node.get("@type"))) {
                    result = node;
                    break;
                }
            }
        }
        return Optional.ofNullable(result);
    }



    private double getCoverageFromCounter(@Nonnull final Node pCounter)
    {
        int missed = Integer.parseInt((String) pCounter.get("@missed"));
        int covered = Integer.parseInt((String) pCounter.get("@covered"));
        return ((double) covered / (missed + covered)) * 100;
    }
}
