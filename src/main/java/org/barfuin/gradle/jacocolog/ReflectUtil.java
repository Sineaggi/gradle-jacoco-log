/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.reflect.MethodUtils;


/**
 * Access certain classes and methods reflectively which may not exist in some versions of Gradle.
 */
public final class ReflectUtil
{
    private ReflectUtil()
    {
        // utility class
    }



    public static boolean hasMethod(final Object pObject, final String pMethodName, final Class<?>... pParamTypes)
    {
        try {
            return MethodUtils.getAccessibleMethod(pObject.getClass(), pMethodName, pParamTypes) != null;
        }
        catch (RuntimeException e) {
            return false;
        }
    }



    /**
     * Call the given parameterless method on the given object if it exists and return the result.
     *
     * @param pObject an object
     * @param pMethodName the name of a parameterless method
     * @return the method's return value. A result of <code>null</code> may mean that the method did not exist, or
     * its return value was <code>null</code>.
     */
    @CheckForNull
    public static Object callMethodIfPresent(@Nullable final Object pObject, @Nonnull final String pMethodName)
    {
        Object result = null;
        if (pObject != null) {
            try {
                Method method = MethodUtils.getAccessibleMethod(pObject.getClass(), pMethodName);
                if (method != null) {
                    result = method.invoke(pObject);
                }
            }
            catch (IllegalAccessException | InvocationTargetException | RuntimeException e) {
                // ignore
            }
        }
        return result;
    }
}
