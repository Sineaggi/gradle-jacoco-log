/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import javax.annotation.Nonnull;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.file.FileCollection;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.testing.Test;
import org.gradle.testing.jacoco.plugins.JacocoPlugin;
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension;
import org.gradle.testing.jacoco.tasks.JacocoReport;


/**
 * Configure the <code>jacocoAggregatedReport</code> task provided by this plugin
 */
public class JacocoAggReportConfigAction
    implements Action<Project>
{
    public static final String JACOCO_AGG_REPORT_TASK_NAME = "jacocoAggregatedReport";



    @Override
    public void execute(@Nonnull final Project pProject)
    {
        final ActionableList<JacocoReport> reportTaskList = new ActionableList<>();
        final ActionableList<Task> testTaskList = new ActionableList<>();
        final ActionableList<FileCollection> classDirList = new ActionableList<>();
        final ActionableList<FileCollection> sourceDirList = new ActionableList<>();

        forAllJacocoEnabledTestTasks(pProject, testTaskList::add);
        forAllReportTasks(pProject, reportTaskList::add); // CHECK This might need to be performed afterEvaluate(), too.

        final TaskProvider<JacocoReport> aggTaskProvider = pProject.getTasks().register(JACOCO_AGG_REPORT_TASK_NAME,
            JacocoReport.class);
        aggTaskProvider.configure(aggReportTask -> {
            pProject.getLogger().info(JacocoLogPlugin.PLUGIN_ID + "[configureAggregatedReportTask]: Configuring '"
                + aggReportTask.getPath() + "'");
            aggReportTask.setGroup(JavaBasePlugin.VERIFICATION_GROUP);
            aggReportTask.setDescription("Generates an aggregated report on this project and all subprojects, "
                + "including results from all test tasks that actually ran");

            testTaskList.all(testTask -> {
                pProject.getLogger().info(JacocoLogPlugin.PLUGIN_ID + "[configureAggregatedReportTask]: "
                    + "Adding execution data from task '" + testTask.getPath() + "'");
                aggReportTask.executionData(ifExists(aggReportTask, testTask));
                aggReportTask.mustRunAfter(testTask);
            });
            classDirList.all(aggReportTask.getClassDirectories()::from);
            sourceDirList.all(aggReportTask.getSourceDirectories()::from);

            aggReportTask.doFirst(new ActionClosure<JacocoReport>(aggReportTask, rpt ->
            {
                rpt.getLogger().info("Task '" + rpt.getPath() + "' execution phase configuration:");
                rpt.getLogger().info("  - Execution Data Files:");
                rpt.getExecutionData().getFiles().forEach(f -> rpt.getLogger().info("      - " + f
                    + (f.canRead() ? " (exists)" : " (missing)")));
                rpt.getLogger().info("  - Class Directories or Class Files:");
                rpt.getAllClassDirs().getFiles().forEach(f -> rpt.getLogger().info("      - " + f));
                rpt.getLogger().info("  - Source Directories or Source Files:");
                rpt.getAllSourceDirs().getFiles().forEach(f -> rpt.getLogger().info("      - " + f));
            }));
        });

        for (final Project aggProject : getProjectsForAggregation(pProject)) {
            aggProject.afterEvaluate(p -> afterEvaluate(p, reportTaskList, classDirList, sourceDirList));
        }
    }



    /**
     * Wraps one execution data file in a closure which checks if the file actually exists.
     * <p>This is necessary in order to lazily determine if the execution data file exists. We must do this only
     * when the file is actually needed, because it will be created at an unknown point of time in the future.</p>
     *
     * @param pAggregatedReportTask our aggregated report task
     * @param pTestTask a task of which we already know for sure that it has a {@link JacocoTaskExtension}. So it is
     * probably a {@link Test} task.
     * @return the closure, which can be passed to {@link JacocoReport#executionData(Object...)}
     */
    private Closure<File> ifExists(final JacocoReport pAggregatedReportTask, final Task pTestTask)
    {
        return new Closure<File>(pAggregatedReportTask)
        {
            @Override
            @SuppressWarnings("MethodDoesntCallSuperMethod")
            public File call()
            {
                final JacocoTaskExtension jacocoExt = pTestTask.getExtensions().getByType(JacocoTaskExtension.class);
                final File execFile = jacocoExt.getDestinationFile();
                if (execFile != null && execFile.canRead()) {
                    return execFile;
                }
                return null;
            }
        };
    }



    private void afterEvaluate(@Nonnull final Project pProject,
        @Nonnull final ActionableList<JacocoReport> pReportTaskList,
        @Nonnull final ActionableList<FileCollection> pClassDirList,
        @Nonnull final ActionableList<FileCollection> pSourceDirList)
    {
        /*
         * This must happen afterEvaluate(), because otherwise the JacocoReport task may not be configured yet, and
         * we'd miss out on the information. For the same reason, it must happen in the afterEvaluate() hook of the
         * project the JacocoReport task belongs to.
         */
        pReportTaskList.all(reportTask -> {
            if (reportTask.getProject().getPath().equals(pProject.getPath())) {
                if (reportTask.isEnabled()) {
                    pProject.getLogger().info(logPrefix(pProject.getPath()) + "afterEvaluate(): "
                        + "Adding directories from report task '" + reportTask.getPath() + "'");
                    pClassDirList.add(reportTask.getAllClassDirs().filter(Objects::nonNull));
                    pSourceDirList.add(reportTask.getAllSourceDirs().filter(Objects::nonNull));
                }
                else {
                    pProject.getLogger().info(logPrefix(pProject.getPath()) + "afterEvaluate(): "
                        + "Report task '" + reportTask.getPath()
                        + "' is not enabled, so we disregard its configuration.");
                }
            }
        });
    }



    private String logPrefix(@Nonnull final String pLocationIndicator)
    {
        return JacocoLogPlugin.PLUGIN_ID + "[" + pLocationIndicator + "] - ";
    }



    private void forAllJacocoEnabledTestTasks(@Nonnull final Project pProject, @Nonnull final Consumer<Task> pAction)
    {
        for (Project project : getProjectsForAggregation(pProject)) {
            project.getPlugins().withType(JacocoPlugin.class).configureEach(jacocoPlugin ->
                project.getTasks().withType(Test.class).configureEach(testTask -> {
                    if (testTask.getExtensions().findByType(JacocoTaskExtension.class) != null) {
                        pProject.getLogger().info(
                            JacocoLogPlugin.PLUGIN_ID + "[forAllJacocoEnabledTestTasks]: Adding test task '"
                                + testTask.getPath() + "' to known test task list because it was configured.");
                        pAction.accept(testTask);
                    }
                })
            );
        }
    }



    private boolean isOurAggregationTask(@Nonnull final JacocoReport pReportTask)
    {
        return JACOCO_AGG_REPORT_TASK_NAME.equals(pReportTask.getName());
    }



    /**
     * Invokes the specified <code>pAction</code> on every task of type {@link JacocoReport} in any of the subprojects
     * of the given project, or the given project itself. This is used for collecting the report tasks.
     *
     * @param pProject the project where <i>gradle-jacoco-log</i> plugin was applied
     * @param pAction the action to execute on each report task
     */
    private void forAllReportTasks(@Nonnull final Project pProject, @Nonnull final Consumer<JacocoReport> pAction)
    {
        final Action<JacocoReport> decoratedAction = (JacocoReport reportTask) -> {
            if (!isOurAggregationTask(reportTask)) {
                pProject.getLogger().info(JacocoLogPlugin.PLUGIN_ID + "[forAllReportTasks]: Adding report task '"
                    + reportTask.getPath() + "' to known report task list.");
                pAction.accept(reportTask);
            }
        };
        for (Project project : getProjectsForAggregation(pProject)) {
            /*
             * Finding the report tasks will cause all tasks of all projects to be realized (configured). There seems
             * to be no way around this, because we must have the configuration of the report tasks if they don't
             * run themselves.
             * CHECK Maybe we should configure them on the aggregation task, and not scan for them? Then they could
             *       be accessed by name, which would avoid realizing unrelated tasks.
             */
            if (pProject.getPath().equals(project.getPath())) {
                /*
                 * For the main project, Jacoco is often added only by our plugin, which appears to cause the Jacoco-
                 * related tasks to appear much later. Therefore, we need to use a slightly different idiom to get
                 * at them.
                 */
                project.getPlugins().withType(JacocoPlugin.class).configureEach(jacocoPlugin ->
                    project.getTasks().withType(JacocoReport.class, decoratedAction));
            }
            else {
                /*
                 * The subprojects might be evaluated earlier than our main project, or much later. Especially, the
                 * 'jacoco' plugin might not have been applied to the subprojects at the time of our evaluation.
                 * Therefore, we must asynchronously inspect every task added to them in order to find the report
                 * tasks.
                 */
                project.getTasks().withType(JacocoReport.class).whenTaskAdded(decoratedAction);
            }
        }
    }



    /**
     * Determine the set of Gradle projects involved in report aggregation, which is this project and all its
     * subprojects. This method relies entirely on information made available in Gradle's <em>init</em> phase.
     *
     * @param pProject this project as passed to apply()
     * @return the set of projects
     */
    private Set<Project> getProjectsForAggregation(final Project pProject)
    {
        Set<Project> result = new HashSet<>(pProject.getSubprojects());
        result.add(pProject);
        return result;
    }
}
