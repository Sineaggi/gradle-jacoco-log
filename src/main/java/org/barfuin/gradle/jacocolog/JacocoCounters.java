/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.Serializable;
import java.util.EnumSet;

import org.gradle.api.tasks.Input;


/**
 * Options for controlling the coverage logging behavior.
 */
public class JacocoCounters
    implements Serializable
{
    private static final long serialVersionUID = 1L;

    private EnumSet<JacocoCounterType> showCoverageFlags = EnumSet.allOf(JacocoCounterType.class);



    @Input
    public boolean isShowClassCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Class);
    }



    public void setShowClassCoverage(final boolean pShowClassCoverage)
    {
        setFlag(JacocoCounterType.Class, pShowClassCoverage);
    }



    public void showClassCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Class);
    }



    @Input
    public boolean isShowMethodCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Method);
    }



    public void setShowMethodCoverage(final boolean pShowMethodCoverage)
    {
        setFlag(JacocoCounterType.Method, pShowMethodCoverage);
    }



    public void showMethodCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Method);
    }



    @Input
    public boolean isShowBranchCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Branch);
    }



    public void setShowBranchCoverage(final boolean pShowBranchCoverage)
    {
        setFlag(JacocoCounterType.Branch, pShowBranchCoverage);
    }



    public void showBranchCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Branch);
    }



    @Input
    public boolean isShowLineCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Line);
    }



    public void setShowLineCoverage(final boolean pShowLineCoverage)
    {
        setFlag(JacocoCounterType.Line, pShowLineCoverage);
    }



    public void showLineCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Line);
    }



    @Input
    public boolean isShowInstructionCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Instruction);
    }



    public void setShowInstructionCoverage(final boolean pShowInstructionCoverage)
    {
        setFlag(JacocoCounterType.Instruction, pShowInstructionCoverage);
    }



    public void showInstructionCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Instruction);
    }



    @Input
    public boolean isShowComplexityCoverage()
    {
        return isCounterEnabled(JacocoCounterType.Complexity);
    }



    public void setShowComplexityCoverage(final boolean pShowComplexityCoverage)
    {
        setFlag(JacocoCounterType.Complexity, pShowComplexityCoverage);
    }



    public void showComplexityCoverageOnly()
    {
        showCoverageFlags = EnumSet.of(JacocoCounterType.Complexity);
    }



    public boolean isCounterEnabled(final JacocoCounterType pType)
    {
        return showCoverageFlags.contains(pType);
    }



    private void setFlag(final JacocoCounterType pType, final boolean pFlag)
    {
        if (pFlag) {
            showCoverageFlags.add(pType);
        }
        else {
            showCoverageFlags.remove(pType);
        }
    }
}
