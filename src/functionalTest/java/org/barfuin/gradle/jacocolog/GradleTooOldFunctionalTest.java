/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


/**
 * Tests that we properly complain if the Gradle version we're run on is too old.
 */
public class GradleTooOldFunctionalTest
{
    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder();



    @Test
    public void testGradleTooOld()
        throws IOException
    {
        final File projectDir = testProjectDir.getRoot();
        Files.createDirectories(projectDir.toPath());
        writeString(new File(projectDir, "settings.gradle"), "");
        writeString(new File(projectDir, "build.gradle"),
            "plugins {"
                + "  id('org.barfuin.gradle.jacocolog')"
                + "}");

        GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withGradleVersion("4.10.2");    // older than our minimum version
        runner.withArguments("tasks");
        runner.withProjectDir(projectDir);
        runner.withDebug(true);
        BuildResult buildResult = runner.buildAndFail();
        String outputLog = unixifyLineBreaks(buildResult.getOutput());

        Assert.assertTrue(outputLog.contains(
            "The plugin 'org.barfuin.gradle.jacocolog' requires at least Gradle 5.0 to be run."));
    }



    private void writeString(final File pFile, final String pString)
        throws IOException
    {
        try (Writer writer = new FileWriter(pFile)) {
            writer.write(pString);
        }
    }



    private String unixifyLineBreaks(final String pLines)
    {
        Assert.assertNotNull(pLines);
        return pLines.replaceAll(Pattern.quote("\r\n") + '?', Matcher.quoteReplacement("\n"));
    }
}
