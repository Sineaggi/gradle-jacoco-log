/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.tools4j.spockito.Spockito;


/**
 * Functional tests for this plugin, which means tests that actually run Gradle via TestKit.
 */
@Spockito.Unroll({
    "| GradleVersion |",
    "|---------------|",
    "|         5.0   |",
    "|         5.6.4 |",
    "|         6.9.2 |",
    "|         7.4.2 |"
})
@RunWith(Spockito.class)
public class FunctionalTestSuite
{
    private static final File FT_BUILD_FOLDER = new File("build/functionalTest");

    public final TemporaryFolder testProjectDir = new TemporaryFolder();

    private final ArchiveTestDataOnFailure testDirArchiver = new ArchiveTestDataOnFailure();

    @Rule
    public TestRule chain = RuleChain.outerRule(testProjectDir).around(testDirArchiver);

    @Spockito.Ref
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private String gradleVersion;



    @BeforeClass
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void createBuildFolder()
    {
        FT_BUILD_FOLDER.mkdirs();
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup01_Default()
        throws IOException
    {
        arrangeTestProject("setup01-default");
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup01_Default_report()
        throws IOException
    {
        arrangeTestProject("setup01-defaultReport", "setup01-default");
        final String actualLog = act(true, "check", "jacocoTestReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup02_LineCoverageOnly()
        throws IOException
    {
        arrangeTestProject("setup02-lineCoverageOnly");
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Test Coverage:\n    - Line Coverage: 80%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup03_brokenXml()
        throws IOException
    {
        arrangeTestProject("setup03-brokenXml");
        final String actualLog = act(false,
            "-Pbarfuin_xmlFile=jacocoTestReport-broken.xml", "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Caused by: org.xml.sax.SAXParseException");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup04_MissingXml()
        throws IOException
    {
        arrangeTestProject("setup04-missingXml");
        final String actualLog = act(false, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "JaCoCo XML report not found:");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup05_MultipleSourceSets()
        throws IOException
    {
        arrangeTestProject("setup05-multipleTestTasks");
        final String actualLog = act(true, "jacocoFooReport", "jacocoBarReport", "jacocoBazReport");

        // Assert that all three test tasks and the default test task have been executed and jacoco info created
        assertFileExists("build/jacoco/fooTest.exec");
        assertFileExists("build/jacoco/barTest.exec");
        assertFileExists("build/jacoco/bazTest.exec");
        assertFileAbsent("build/jacoco/test.exec");  // task 'test' was not executed

        // Assert that jacoco reports have been created for all test tasks (XML), HTML only when not disabled
        assertFileAbsent("build/reports/jacoco/jacocoFooReport/html");
        assertFileExists("build/reports/jacoco/jacocoFooReport/jacocoFooReport.xml");
        assertFileExists("build/reports/jacoco/jacocoBarReport/html");
        assertFileExists("build/reports/jacoco/jacocoBarReport/jacocoBarReport.xml");
        assertFileExists("build/reports/jacoco/jacocoBazReport/html");
        assertFileExists("build/reports/jacoco/jacocoBazReport/jacocoBazReport.xml");
        assertFileAbsent("build/reports/jacoco/jacocoTestReport");

        // Assert that correct log output has been created by our plugin
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogFooCoverage");
        assertLogged(actualLog, "Test Coverage:\n    - Branch Coverage: 50%\n    - Instruction Coverage: 80.6%\n"
            + ":jacocoLogBarCoverage");
        assertNumMatches(2, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    public void testSetup06_aggregateSubprojects()
        throws IOException
    {
        arrangeTestProject("setup06-aggregateSubprojects");
        final String actualLog = act(true, "test", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 75%\n    - Method Coverage: 75%\n"
            + "    - Branch Coverage: 37.5%\n    - Line Coverage: 57.7%\n    - Instruction Coverage: 56.7%\n"
            + "    - Complexity Coverage: 42.9%\n:jacocoLogAggregatedCoverage");
        assertNumMatches(1, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    public void testSetup06_aggregateSubprojects2()
        throws IOException
    {
        arrangeTestProject("setup06-aggregateSubprojects2", "setup06-aggregateSubprojects");
        final String actualLog = act(true, ":module1:test", ":module2:test", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 50%\n    - Method Coverage: 50%\n"
            + "    - Branch Coverage: 25%\n    - Line Coverage: 42.3%\n    - Instruction Coverage: 40.6%\n"
            + "    - Complexity Coverage: 28.6%\n:jacocoLogAggregatedCoverage");
        assertNumMatches(1, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    public void testSetup06_aggregateSubprojects3()
        throws IOException
    {
        arrangeTestProject("setup06-aggregateSubprojects3", "setup06-aggregateSubprojects");
        final String actualLog = act(true, ":module1:test", ":module4-noTests:test", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 25%\n    - Method Coverage: 25%\n"
            + "    - Branch Coverage: 12.5%\n    - Line Coverage: 19.2%\n    - Instruction Coverage: 17.8%\n"
            + "    - Complexity Coverage: 14.3%\n:jacocoLogAggregatedCoverage");
        assertNumMatches(1, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup06_configurationAvoidance()
        throws IOException
    {
        arrangeTestProject("setup06-configurationAvoidance", "setup06-aggregateSubprojects");
        final String actualLog = act(true, "--debug", "compileJava");

        final Pattern realizedTasksPattern = Pattern.compile("Build operation 'Realize task ([^']+)' started");
        final Matcher matcher = realizedTasksPattern.matcher(actualLog);
        final SortedSet<String> actualRealizedTasks = new TreeSet<>();
        while (matcher.find()) {
            actualRealizedTasks.add(matcher.group(1));
        }
        final SortedSet<String> expectedRealizedTasks = new TreeSet<>(Arrays.asList(
            ":clean",
            ":compileJava",
            ":jacocoAggregatedReport",
            ":jacocoTestReport",
            ":module1:clean",
            ":module1:compileJava",
            ":module1:jacocoTestReport",
            ":module1:test",
            ":module2:clean",
            ":module2:compileJava",
            ":module2:jacocoTestReport",
            ":module2:test",
            ":module3-noJacoco:clean",
            ":module3-noJacoco:compileJava",
            ":module4-noTests:clean",
            ":module4-noTests:compileJava",
            ":module4-noTests:jacocoTestReport",
            ":module4-noTests:test",
            ":test"));

        Assert.assertEquals(expectedRealizedTasks, actualRealizedTasks);
        assertNotLogged(actualLog, "warn");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup07_doesNotAggregateWhenAppliedOnSubprojects()
        throws IOException
    {
        arrangeTestProject("setup07-doNotAggregateSubprojects");
        final String actualLog = act(true, "test", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 71.4%\n    - Instruction Coverage: 66.7%\n"
            + "    - Complexity Coverage: 57.1%\n:module1:jacocoLogAggregatedCoverage");
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 66.7%\n    - Instruction Coverage: 68.3%\n"
            + "    - Complexity Coverage: 57.1%\n:module2:jacocoLogAggregatedCoverage");

        assertLogged(actualLog, "jacocoLogAggregatedCoverage: The report task which this task is associated with "
            + "(':module4-noTests:jacocoAggregatedReport') did not perform any work.");
        assertNotLogged(actualLog, "Coverage: 0%\n:module4-noTests:jacocoLogAggregatedCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup08_aggregateReconfiguredSubprojects()
        throws IOException
    {
        arrangeTestProject("setup08-aggregateReconfigured");
        final String actualLog = act(true, "check", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 65.5%\n    - Instruction Coverage: 62.5%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogAggregatedCoverage");
        assertNumMatches(1, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup09_noTests()
        throws IOException
    {
        arrangeTestProject("setup09-noTests");
        final String actualLog = act(true, "build");

        assertLogged(actualLog, "no report XML exists, so we skip coverage logging");
        assertNotLogged(actualLog, "Test Coverage:");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup10_upToDateReportTask_dontLog()
        throws IOException
    {
        arrangeTestProject("setup10-dontLogWhenUpToDate");
        act(true, "check", "jacocoLogTestCoverage");                           // run once
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");  // run again, so most tasks up-to-date

        assertLogged(actualLog, "Consequently, we skip coverage logging, too.");
        assertNotLogged(actualLog, "Test Coverage:");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup11_missingCounters()
        throws IOException
    {
        arrangeTestProject("setup11-missingCounters");
        final String actualLog = act(true, "check", "jacocoTestReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 100%\n    - Line Coverage: 100%\n    - Instruction Coverage: 100%\n"
            + "    - Complexity Coverage: 100%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup12_xmlReportTurnedOff()
        throws IOException
    {
        arrangeTestProject("setup12-xmlReportTurnedOff");
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup13_emptyXml()
        throws IOException
    {
        arrangeTestProject("setup13-emptyXml");
        final String actualLog = act(true,
            "-Pbarfuin_xmlFile=jacocoTestReport-empty.xml", "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "WARNING: No counters found in JaCoCo XML report");
        assertNotLogged(actualLog, "Test Coverage:");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup14_evaluationDependsOnChildren()
        throws IOException
    {
        arrangeTestProject("setup14-evaluationDependsOnChildren");
        final String actualLog = act(true, "test", "jacocoAggregatedReport");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 75%\n    - Method Coverage: 75%\n"
            + "    - Branch Coverage: 37.5%\n    - Line Coverage: 57.7%\n    - Instruction Coverage: 56.7%\n"
            + "    - Complexity Coverage: 42.9%\n:jacocoLogAggregatedCoverage");
        assertNumMatches(1, actualLog, "Test Coverage:\n");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup15_Precision2()
        throws IOException
    {
        arrangeTestProject("testSetup15_Precision2", "setup15-precision");
        final String actualLog = act(true, "-PmaxDecimalDigits=2", "check", "jacocoTestReport");

        assertNotLogged(actualLog, "WARNING: Invalid value ");
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.56%\n"
            + "    - Complexity Coverage: 57.14%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup15_Precision20()
        throws IOException
    {
        arrangeTestProject("testSetup15_Precision20", "setup15-precision");
        final String actualLog = act(true, "-PmaxDecimalDigits=20", "check", "jacocoTestReport");

        // quietly use only 10 decimal digits
        assertNotLogged(actualLog, "WARNING: Invalid value ");
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.5555555556%\n"
            + "    - Complexity Coverage: 57.1428571429%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup15_PrecisionMinus1()
        throws IOException
    {
        arrangeTestProject("testSetup15_PrecisionMinus1", "setup15-precision");
        final String actualLog = act(true, "-PmaxDecimalDigits=-1", "check", "jacocoTestReport");

        assertLogged(actualLog, "WARNING: Invalid value (-1) in jacocoLogTestCoverage.maxDecimalDigits, defaulting ");
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    @Test
    @Spockito.Name(" [Gradle {GradleVersion}]")
    //@Ignore
    public void testSetup16_upToDateReportTask()
        throws IOException
    {
        arrangeTestProject("setup01-default");
        act(true, "check", "jacocoLogTestCoverage");                           // run once
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");  // run again, so most tasks up-to-date

        assertLogged(actualLog, "Still, the report XML exists");
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    private void arrangeTestProject(final String pName)
        throws IOException
    {
        arrangeTestProject(pName, pName);
    }



    private void arrangeTestProject(final String pTestName, final String pSetupDirName)
        throws IOException
    {
        final File testDir = testProjectDir.getRoot();
        final File srcDir = getSourceDir(pSetupDirName);
        FileUtils.copyDirectory(srcDir, testDir);

        final File props = new File("build/tmp/testprops/gradle.properties");
        if (!props.canRead()) {
            Assert.fail("Prerequisite Gradle task 'testprops' not run - "
                + "execute functional tests via Gradle to ensure correct environment");
        }
        FileUtils.copyFileToDirectory(props, testDir);

        testDirArchiver.setTestDir(testDir);
        testDirArchiver.setTargetZip(new File(FT_BUILD_FOLDER, pTestName + "-FAILED.zip"));
        testDirArchiver.setOutputLog(null);

        System.out.println("Test project set up in: " + testDir);
    }



    private File getSourceDir(final String pSetupName)
    {
        return new File("src/functionalTest/resources", pSetupName);
    }



    private String act(final boolean pExpectSuccess, final String... pCommandLineArgs)
    {
        final GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withProjectDir(testProjectDir.getRoot());
        runner.withGradleVersion(gradleVersion);
        runner.withArguments(Stream.concat(
            Arrays.stream(new String[]{"--info", "--stacktrace"}),
            Arrays.stream(pCommandLineArgs)).toArray(String[]::new));
        //runner.withDebug(true);

        BuildResult buildResult = pExpectSuccess ? runner.build() : runner.buildAndFail();

        String buildLog = buildResult.getOutput();
        testDirArchiver.setOutputLog(buildLog);
        return buildLog;
    }



    private void assertLogged(final String pActualLog, final String pExpectedLogOutput)
    {
        final String actualLogUnix = unixifyLog(pActualLog);
        Assert.assertTrue(actualLogUnix.contains(pExpectedLogOutput));
    }



    private void assertNotLogged(final String pActualLog, final String pForbiddenLogOutput)
    {
        final String actualLogUnix = unixifyLog(pActualLog);
        Assert.assertFalse(actualLogUnix.contains(pForbiddenLogOutput));
    }



    private void assertFileExists(final String pRelativePath)
    {
        Assert.assertTrue("Expected file or directory not found: " + pRelativePath,
            new File(testProjectDir.getRoot(), pRelativePath).canRead());
    }



    private void assertFileAbsent(final String pRelativePath)
    {
        Assert.assertFalse("File or directory found which should not be present: " + pRelativePath,
            new File(testProjectDir.getRoot(), pRelativePath).exists());
    }



    private void assertNumMatches(final int pExpectedCount, final String pActualLog, final String pSub)
    {
        final String actualLogUnix = unixifyLog(pActualLog);
        int count = 0;
        if (!actualLogUnix.isEmpty()) {
            for (int idx = 0; (idx = actualLogUnix.indexOf(pSub, idx)) != -1; idx += pSub.length()) {
                ++count;
            }
        }
        Assert.assertEquals(pExpectedCount, count);
    }



    private String unixifyLog(final String pActualLog)
    {
        Assert.assertNotNull(pActualLog);
        return pActualLog.replaceAll(Pattern.quote("\r\n") + '?', Matcher.quoteReplacement("\n"));
    }
}
